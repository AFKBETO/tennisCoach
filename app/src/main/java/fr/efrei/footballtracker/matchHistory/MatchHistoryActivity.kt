package fr.efrei.footballtracker.matchHistory

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.firebase.auth.FirebaseAuth
import fr.efrei.footballtracker.MainActivity
import fr.efrei.footballtracker.firebaseRepositories.Match
import fr.efrei.footballtracker.R
import fr.efrei.footballtracker.databinding.ActivityMatchHistoryBinding
import fr.efrei.footballtracker.firebaseRepositories.MatchFirebaseService
import fr.efrei.footballtracker.repositories.MatchDatabase
import fr.efrei.footballtracker.repositories.MatchSqlService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MatchHistoryActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMatchHistoryBinding
    private lateinit var matchFirebaseService: MatchFirebaseService
    private lateinit var db: MatchDatabase
    private lateinit var auth: FirebaseAuth
    var selectedMatch: Match? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        WindowCompat.setDecorFitsSystemWindows(window, false)
        super.onCreate(savedInstanceState)

        binding = ActivityMatchHistoryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        db = MatchSqlService.getInstance()
        matchFirebaseService = MatchFirebaseService
        auth = FirebaseAuth.getInstance()

        setSupportActionBar(binding.matchHistoryBar)

        val navController = findNavController(R.id.nav_host_fragment_content_match_history)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        navController.addOnDestinationChangedListener { _, _, _ ->
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.baseline_navigate_before_36)
        }

        binding.matchHistoryBar.apply {

            setNavigationOnClickListener {
                if (navController.currentDestination?.id == R.id.MatchHistoryFragment1) {
                    val intent = Intent(this@MatchHistoryActivity, MainActivity::class.java)
                    startActivity(intent)
                } else {
                    navController.navigateUp()
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_match_history)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }
}