package fr.efrei.footballtracker.matchHistory

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import fr.efrei.footballtracker.firebaseRepositories.Match
import fr.efrei.footballtracker.R

class MatchAdapter(private val onClick: (Match) -> Unit): ListAdapter<Match, MatchAdapter.MatchViewHolder>(MatchDiffCallback) {

    class MatchViewHolder(matchView: View, val onClick: (Match) -> Unit): RecyclerView.ViewHolder(matchView) {
        private val titleView: TextView = matchView.findViewById(R.id.txt_sum_title)
        private val dateView: TextView = matchView.findViewById(R.id.txt_sum_date)
        private val addressView: TextView = matchView.findViewById(R.id.txt_sum_address)
        private val team1View: TextView = matchView.findViewById(R.id.txt_sum_team1)
        private val team2View: TextView = matchView.findViewById(R.id.txt_sum_team2)
        private var currentMatch: Match? = null

        init {
            matchView.setOnClickListener {
                currentMatch?.let {
                    onClick(it)
                }
            }
        }

        fun bind(match: Match) {
            currentMatch = match
            titleView.text = match.title
            dateView.text = match.date
            addressView.text = match.location
            team1View.text = match.team1
            team2View.text = match.team2
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.match_summary, parent, false)
        return MatchViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        val match = getItem(position)
        holder.bind(match)

    }
}

object MatchDiffCallback : DiffUtil.ItemCallback<Match>() {
    override fun areItemsTheSame(oldItem: Match, newItem: Match): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Match, newItem: Match): Boolean {
        return oldItem == newItem
    }
}