package fr.efrei.footballtracker.matchHistory

import android.content.res.Configuration.ORIENTATION_PORTRAIT
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.auth.FirebaseAuth
import fr.efrei.footballtracker.createMatch.ImageAdapter
import fr.efrei.footballtracker.databinding.FragmentMatchHistorySecondBinding
import fr.efrei.footballtracker.firebaseRepositories.Match
import fr.efrei.footballtracker.firebaseRepositories.MatchFirebaseService
import fr.efrei.footballtracker.repositories.MatchSqlService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class MatchHistoryFragment2 : Fragment() {

    private var _binding: FragmentMatchHistorySecondBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentMatchHistorySecondBinding.inflate(inflater, container, false)
        lifecycleScope.launch(Dispatchers.IO) {
            var match = (activity as MatchHistoryActivity).selectedMatch
            if (match == null) {
                val db = MatchSqlService.getInstance()
                val auth = FirebaseAuth.getInstance()
                val matchId = savedInstanceState?.getString("selectedMatch")
                match = if (auth.currentUser == null) {
                    Match.convertToFirebase(db.matchDao().getById(matchId!!))
                } else {
                    MatchFirebaseService.getMatch(matchId!!)
                }

            }
            launch(Dispatchers.Main) {
                binding.matchTitle.text = match?.title
                binding.matchDate.text = match?.date
                binding.matchLocation.text = match?.location
                val matchResult = "${match?.score1} - ${match?.score2}"
                binding.matchResult.text = matchResult
                binding.matchTeam1.text = match?.team1
                binding.matchTeam2.text = match?.team2
                binding.matchTeam1Corners.text = match?.corners1.toString()
                binding.matchTeam2Corners.text = match?.corners2.toString()
                binding.matchTeam1Fouls.text = match?.fouls1.toString()
                binding.matchTeam2Fouls.text = match?.fouls2.toString()
                binding.matchTeam1Offsides.text = match?.offsides1.toString()
                binding.matchTeam2Offsides.text = match?.offsides2.toString()
                binding.matchTeam1RedCards.text = match?.redCards1.toString()
                binding.matchTeam2RedCards.text = match?.redCards2.toString()
                binding.matchTeam1Shots.text = match?.shots1.toString()
                binding.matchTeam2Shots.text = match?.shots2.toString()
                binding.matchTeam1YellowCards.text = match?.yellowCards1.toString()
                binding.matchTeam2YellowCards.text = match?.yellowCards2.toString()
                val isPortrait = resources.configuration.orientation == ORIENTATION_PORTRAIT
                val spanCount = if (isPortrait) 2 else 4
                val gridLayoutManager = GridLayoutManager(requireContext(), spanCount)
                val imageAdapter = ImageAdapter(match!!.photos)
                binding.historyRecyclerView.adapter = imageAdapter
                binding.historyRecyclerView.layoutManager = gridLayoutManager
            }
        }


        return binding.root

    }
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val selectedMatch = (activity as MatchHistoryActivity).selectedMatch
        if (selectedMatch != null) {
            outState.putString("selectedMatch", selectedMatch.id)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /*binding.buttonSecond.setOnClickListener {
            findNavController().navigate(R.id.action_Second2Fragment_to_First2Fragment)
        }*/
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}