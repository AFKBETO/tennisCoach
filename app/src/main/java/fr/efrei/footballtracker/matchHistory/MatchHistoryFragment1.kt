package fr.efrei.footballtracker.matchHistory

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import fr.efrei.footballtracker.firebaseRepositories.Match
import fr.efrei.footballtracker.firebaseRepositories.MatchFirebaseService
import fr.efrei.footballtracker.R
import fr.efrei.footballtracker.databinding.FragmentMatchHistoryFirstBinding
import fr.efrei.footballtracker.repositories.MatchDatabase
import fr.efrei.footballtracker.repositories.MatchSqlService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class MatchHistoryFragment1 : Fragment() {

    private var _binding: FragmentMatchHistoryFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private lateinit var matchFirebaseService: MatchFirebaseService
    private lateinit var db: MatchDatabase
    private lateinit var auth: FirebaseAuth
    private lateinit var matches: List<Match>


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentMatchHistoryFirstBinding.inflate(inflater, container, false)
        val linearLayoutManager = LinearLayoutManager(context)
        db = MatchSqlService.getInstance()
        matchFirebaseService = MatchFirebaseService
        auth = FirebaseAuth.getInstance()
        this.viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                matches = if (auth.currentUser != null) {
                    val result = matchFirebaseService.getMatches()
                    result.ifEmpty {
                        val matchesSql = db.matchDao().getAll()
                        matchesSql.map { matchSql ->
                            Match.convertToFirebase(matchSql)
                        }
                    }
                } else {
                    val matchesSql = db.matchDao().getAll()
                    matchesSql.map { matchSql ->
                        Match.convertToFirebase(matchSql)
                    }
                }
                launch(Dispatchers.Main) {
                    val matchAdapter = MatchAdapter{ match -> onMatchClicked(match) }
                    matchAdapter.submitList(matches)
                    val recyclerView = binding.recyclerView
                    recyclerView.layoutManager = linearLayoutManager
                    recyclerView.adapter = matchAdapter
                }
            }
        }

        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
    private fun onMatchClicked(match: Match) {
        (activity as MatchHistoryActivity).selectedMatch = match
        findNavController().navigate(R.id.action_First2Fragment_to_Second2Fragment)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}