package fr.efrei.footballtracker.firebaseRepositories

import android.graphics.Bitmap
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.tasks.await
import java.io.ByteArrayOutputStream

object FirebaseStorageService {
    private val auth: FirebaseAuth = FirebaseAuth.getInstance()
    private val storage: FirebaseStorage = FirebaseStorage.getInstance()


    suspend fun uploadPhoto(photo: Bitmap, photoName: String): String {
        val ref = storage.reference.child("${auth.uid}/${photoName}")
        val baos = ByteArrayOutputStream()
        photo.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()
        val uploadTask = ref.putBytes(data)
        return uploadTask.await().storage.downloadUrl.await().toString()
    }
}