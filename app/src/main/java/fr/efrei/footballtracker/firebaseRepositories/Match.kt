package fr.efrei.footballtracker.firebaseRepositories

import kotlinx.serialization.Serializable
import java.text.SimpleDateFormat
import java.util.Date

@Serializable
data class Match (
    var lat: Double = 0.0,
    var lon: Double = 0.0,
    var location: String = "",
    var title: String = "",
    var team1: String = "",
    var team2: String = "",
    var date: String = dateFormat.format(Date()),
    var score1: Int = 0,
    var score2: Int = 0,
    var isFinished: Boolean = false,
    var photos: ArrayList<String> = ArrayList(),
    var shots1: Int = 0,
    var shots2: Int = 0,
    var offsides1: Int = 0,
    var offsides2: Int = 0,
    var corners1: Int = 0,
    var corners2: Int = 0,
    var fouls1: Int = 0,
    var fouls2: Int = 0,
    var yellowCards1: Int = 0,
    var yellowCards2: Int = 0,
    var redCards1: Int = 0,
    var redCards2: Int = 0,
    var createdDate: String = dateFormat.format(Date()),
    var id: String? = null
) {
    companion object {
        private val dateFormat = SimpleDateFormat("yyyy/MM/dd")
        fun convertToSql(match: Match): fr.efrei.footballtracker.repositories.MatchWithPhoto {
            return fr.efrei.footballtracker.repositories.MatchWithPhoto(
                fr.efrei.footballtracker.repositories.Match(
                    match.id!!,
                    match.lat,
                    match.lon,
                    match.location,
                    match.title,
                    match.team1,
                    match.team2,
                    match.date,
                    match.score1,
                    match.score2,
                    match.isFinished,
                    match.shots1,
                    match.shots2,
                    match.offsides1,
                    match.offsides2,
                    match.corners1,
                    match.corners2,
                    match.fouls1,
                    match.fouls2,
                    match.yellowCards1,
                    match.yellowCards2,
                    match.redCards1,
                    match.redCards2,
                    match.createdDate
                ),
                match.photos.map {
                    fr.efrei.footballtracker.repositories.Photo(
                        match.id!!,
                        it,
                        null
                    )
                }.toCollection(ArrayList())
            )
        }
        fun convertToFirebase(match: fr.efrei.footballtracker.repositories.MatchWithPhoto): Match {
            return Match(
                match.match.lat!!,
                match.match.lon!!,
                match.match.location!!,
                match.match.title!!,
                match.match.team1!!,
                match.match.team2!!,
                match.match.date!!,
                match.match.score1!!,
                match.match.score2!!,
                match.match.isFinished!!,
                match.photos.map {
                    it.path!!
                }.toCollection(ArrayList()),
                match.match.shots1!!,
                match.match.shots2!!,
                match.match.offsides1!!,
                match.match.offsides2!!,
                match.match.corners1!!,
                match.match.corners2!!,
                match.match.fouls1!!,
                match.match.fouls2!!,
                match.match.yellowCards1!!,
                match.match.yellowCards2!!,
                match.match.redCards1!!,
                match.match.redCards2!!,
                match.match.createdDate!!,
                match.match.id
            )
        }
    }
}