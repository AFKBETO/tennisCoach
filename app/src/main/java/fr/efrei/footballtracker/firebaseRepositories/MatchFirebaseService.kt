package fr.efrei.footballtracker.firebaseRepositories

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.tasks.await

object MatchFirebaseService {
    private val auth: FirebaseAuth = FirebaseAuth.getInstance()
    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()

    suspend fun createMatch(match: Match): Match {
        println("auth + ${auth.uid}")
        match.id = db.collection(auth.uid!!).document().id
        db.collection(auth.uid!!).document(match.id.toString()).set(match).await()
        return match
    }

    suspend fun getMatch(id: String): Match
        = db.collection(auth.uid!!).document(id).get().await().toObject(Match::class.java)!!

    suspend fun getMatches(): List<Match>
        = db.collection(auth.uid!!).orderBy("createdDate").get().await().toObjects(Match::class.java)

    suspend fun updateMatch(match: Match): Void
        = db.collection(auth.uid!!).document(match.id.toString()).set(match).await()

    suspend fun deleteMatch(match: Match): Void
        = db.collection(auth.uid!!).document(match.id.toString()).delete().await()
}