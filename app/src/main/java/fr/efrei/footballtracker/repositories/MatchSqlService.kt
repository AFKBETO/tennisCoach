package fr.efrei.footballtracker.repositories

import android.content.Context
import androidx.room.Room

object MatchSqlService {
    private lateinit var db: MatchDatabase

    fun initializeDatabase(context: Context) {
        db = Room.databaseBuilder(
            context,
            MatchDatabase::class.java,
            "match-database"
        ).build()
    }

    fun getInstance(): MatchDatabase {
        return db
    }

}