package fr.efrei.footballtracker.repositories

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "photos", indices = [Index(value = ["matchId"])])
data class Photo (
    val matchId: String?,
    val path: String?,
    @PrimaryKey(autoGenerate = true)
    val id: Int?
)