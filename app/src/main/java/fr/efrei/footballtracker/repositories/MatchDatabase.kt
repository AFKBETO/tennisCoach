package fr.efrei.footballtracker.repositories

import androidx.room.Database

@Database(entities = [Match::class, Photo::class], version = 1)
abstract class MatchDatabase: androidx.room.RoomDatabase() {
    abstract fun matchDao(): MatchDao
}