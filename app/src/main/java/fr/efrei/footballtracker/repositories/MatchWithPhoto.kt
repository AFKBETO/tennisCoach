package fr.efrei.footballtracker.repositories

import androidx.room.Embedded
import androidx.room.Relation

data class MatchWithPhoto (
    @Embedded val match: Match,
    @Relation(
        parentColumn = "id",
        entityColumn = "matchId"
    )
    val photos: List<Photo>
)