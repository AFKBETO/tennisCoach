package fr.efrei.footballtracker.repositories

import androidx.room.*

@Dao
abstract class MatchDao {
    @Transaction
    @Query("SELECT * FROM matches " +
            "ORDER BY matches.createdDate DESC")
    abstract suspend fun getAll(): List<MatchWithPhoto>

    @Transaction
    @Query("SELECT * FROM matches " +
            "WHERE id = :id")
    abstract suspend fun getById(id: String): MatchWithPhoto

    @Transaction
    open suspend fun insertMatchesWithPhoto(vararg matches: MatchWithPhoto) {
        matches.forEach {
            insertMatch(it.match)
            it.photos.forEach { photo ->
                insertPhoto(photo)
            }
        }
    }
    open suspend fun insertMatch(match: Match) {
        val matches = getAll()
        if (matches.size >= 5) {
            deleteMatches(matches[matches.size - 1])
        }
        insertMatchInternal(match)
    }
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertMatchInternal(vararg matches: Match)


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertPhoto(vararg photos: Photo)

    @Transaction
    open suspend fun updateMatches(vararg matches: MatchWithPhoto) {
        matches.forEach {
            updateMatch(it.match)
            it.photos.forEach { photo ->
                updatePhoto(photo)
            }
        }
    }

    @Update
    abstract suspend fun updateMatch(vararg matches: Match)

    @Update
    abstract suspend fun updatePhoto(vararg photos: Photo)

    @Transaction
    open suspend fun deleteMatches(vararg matches: MatchWithPhoto) {
        matches.forEach {
            it.photos.forEach { photo ->
                deletePhoto(photo)
            }
            deleteMatch(it.match)
        }
    }

    @Delete
    abstract suspend fun deleteMatch(vararg matches: Match)

    @Delete
    abstract suspend fun deletePhoto(vararg photos: Photo)
}