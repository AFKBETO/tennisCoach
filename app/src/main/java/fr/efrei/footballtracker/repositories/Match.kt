package fr.efrei.footballtracker.repositories

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "matches")
data class Match(
    @PrimaryKey val id: String,
    val lat: Double?,
    val lon: Double?,
    val location: String?,
    val title: String?,
    val team1: String?,
    val team2: String?,
    val date: String?,
    val score1: Int?,
    val score2: Int?,
    val isFinished: Boolean?,
    val shots1: Int?,
    val shots2: Int?,
    val offsides1: Int?,
    val offsides2: Int?,
    val corners1: Int?,
    val corners2: Int?,
    val fouls1: Int?,
    val fouls2: Int?,
    val yellowCards1: Int?,
    val yellowCards2: Int?,
    val redCards1: Int?,
    val redCards2: Int?,
    val createdDate: String?
)