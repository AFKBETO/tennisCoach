package fr.efrei.footballtracker.createMatch

import android.Manifest
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.ContentValues
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.MapsInitializer.Renderer
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.net.PlacesClient
import fr.efrei.footballtracker.firebaseRepositories.Match
import fr.efrei.footballtracker.R
import fr.efrei.footballtracker.databinding.FragmentCreateMatchFirstBinding
import kotlinx.coroutines.*
import java.io.IOException
import java.util.*


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class CreateMatchFragment1 : Fragment(), OnMapsSdkInitializedCallback {

    private var _binding: FragmentCreateMatchFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private var nMap: GoogleMap? = null
    private lateinit var mapView: MapView
    private var lastKnownLocation: Location? = null
    private var cameraPosition: CameraPosition? = null
    private var placesClient: PlacesClient? = null
    private var marker: Marker? = null
    private lateinit var match: Match

    private lateinit var datePickerDialog: DatePickerDialog

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    private var addressTextView: TextView? = null

    // Everything after this is in red
    val red = "\uD83D\uDD34"


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.findViewById<View>(R.id.add_match)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.next_page)?.visibility = View.VISIBLE
        _binding = FragmentCreateMatchFirstBinding.inflate(inflater, container, false)

        MapsInitializer.initialize(this.requireActivity().applicationContext,
            Renderer.LATEST, this)
        mapView = binding.mapView
        addressTextView = binding.txtAddress
        mapView.onCreate(savedInstanceState)
        println(red +"onCreateView")
        mapView.getMapAsync(OnMapReadyCallback {
            println(red +"onMapReady")
            this.initializeMap(it, savedInstanceState)
            placesClient = Places.createClient(this.requireActivity())
        })
        println(red +"onCreateView2")


        return binding.root
    }
    private fun initializeMap(map: GoogleMap, savedInstanceState: Bundle?) {
        println(red +"initializeMap")
        nMap = map
        println(red +"nMap = map")
        nMap!!.uiSettings.isZoomControlsEnabled = true
        nMap!!.uiSettings.isMapToolbarEnabled = false
        println(red +"uiSettings")
        if (ActivityCompat.checkSelfPermission(this.requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            nMap!!.isMyLocationEnabled = true
        }

        nMap!!.setOnMapClickListener { point ->
            val location = Location("Test")
            location.latitude = point.latitude
            location.longitude = point.longitude
            println("location = ${location.latitude} - ${location.longitude}")
            nMap!!.animateCamera(CameraUpdateFactory.newLatLng(point))
            lifecycleScope.launch(Dispatchers.IO) {
                getAddress(location)
                launch (Dispatchers.Main) {
                    marker!!.position = point
                }
            }
        }
        try {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this.requireActivity())
        } catch (e: GooglePlayServicesNotAvailableException) {
            e.printStackTrace()
        }
        if (savedInstanceState != null) {
            val lat = savedInstanceState.getDouble("lat")
            val lng = savedInstanceState.getDouble("lng")
            val location = Location("Test")
            location.latitude = lat
            location.longitude = lng
            nMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), DEFAULT_ZOOM))
            marker = nMap!!.addMarker(MarkerOptions().position(LatLng(lat, lng)))
            lifecycleScope.launch(Dispatchers.IO) {
                getAddress(location)
            }
        } else {
            if (nMap!!.isMyLocationEnabled) {
                this.viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
                    getDeviceLocation()
                }
            } else {
                if (marker != null) {
                    marker!!.remove()
                }
                nMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultLocation, DEFAULT_ZOOM))
                marker = nMap!!.addMarker(MarkerOptions().position(defaultLocation))
                nMap!!.uiSettings.isMyLocationButtonEnabled = false
            }
        }

        binding.txtTeam1.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                match.team1 = s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
        binding.txtTeam2.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                match.team2 = s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
        binding.txtTitle.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                match.title = s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
        binding.txtDate.setOnClickListener {
            val c = Calendar.getInstance()
            val mYear = c[Calendar.YEAR] // current year

            val mMonth = c[Calendar.MONTH] // current month

            val mDay = c[Calendar.DAY_OF_MONTH] // current day
            datePickerDialog = DatePickerDialog(
                this.requireActivity(),
                { _, year, monthOfYear, dayOfMonth -> // set day of month , month and year value in the edit text
                    binding.txtDate.setText(
                        dayOfMonth.toString() + "/"
                                + (monthOfYear + 1) + "/" + year
                    )
                }, mYear, mMonth, mDay
            )
            datePickerDialog.show()
        }
    }
    override fun onMapsSdkInitialized(renderer: Renderer) {
        when (renderer) {
            Renderer.LATEST -> Log.d("MapsDemo", "The latest version of the renderer is used.")
            Renderer.LEGACY -> Log.d("MapsDemo", "The legacy version of the renderer is used.")
        }
    }

    @SuppressLint("MissingPermission")
    private fun getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (ActivityCompat.checkSelfPermission(this.requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                val locationResult = fusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(this.requireActivity()) { task ->
                    if (task.isSuccessful) {
                        // Set the map's camera position to the current location of the device.
                        lastKnownLocation = task.result

                        if (lastKnownLocation != null) {
                            getAddress(lastKnownLocation!!)
                            nMap!!.moveCamera(
                                CameraUpdateFactory.newLatLngZoom(
                                LatLng(lastKnownLocation!!.latitude,
                                    lastKnownLocation!!.longitude), DEFAULT_ZOOM))
                            if (marker != null) {
                                marker!!.remove()
                            }
                            marker = nMap!!.addMarker(MarkerOptions().position(LatLng(lastKnownLocation!!.latitude,
                                lastKnownLocation!!.longitude)))
                        }
                    } else {
                        Log.d(ContentValues.TAG, "Current location is null. Using defaults.")
                        Log.e(ContentValues.TAG, "Exception: %s", task.exception)
                        nMap!!.moveCamera(
                            CameraUpdateFactory
                            .newLatLngZoom(defaultLocation, DEFAULT_ZOOM))
                        if (marker != null) {
                            marker!!.remove()
                        }
                        marker = nMap!!.addMarker(MarkerOptions().position(defaultLocation))
                        nMap!!.uiSettings.isMyLocationButtonEnabled = false
                    }
                }
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        nMap?.animateCamera( CameraUpdateFactory.zoomBy(0f) );
    }
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (marker != null) {
            outState.putDouble("lng", marker!!.position.longitude)
            outState.putDouble("lat", marker!!.position.latitude)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        match = (activity as CreateMatchActivity).match
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mapView.onDestroy()
        println(red +"onDestroyView")
        _binding = null
    }
    override fun onPause() {
        super.onPause()
        println(red +"onPause")
        mapView.onPause()
    }
    override fun onResume() {
        super.onResume()
        println(red +"onResume")
        mapView.onResume()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        println(red +"onLowMemory")
        mapView.onLowMemory()
    }

    private fun getAddress(vararg locations: Location) {
        val geocoder = Geocoder(requireContext(), Locale.getDefault())

        val location: Location = locations[0]

        match.lat = location.latitude
        match.lon = location.longitude

        println("Location: $location")
        var addresses: List<Address>? = null
        try {
            geocoder.getFromLocation(location.latitude, location.longitude,1) {
                addresses = it
                println("Address: $addresses")
                viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
                    match.location = if (addresses != null && addresses!!.isNotEmpty()) {
                        addresses!![0].getAddressLine(0)
                    } else {
                        "No address found"
                    }
                    addressTextView!!.text = match.location
                }
            }
        } catch (ioException: IOException) {
            Toast.makeText(this@CreateMatchFragment1.context,"Unable connect to Geocoder",Toast.LENGTH_LONG).show()
        }


    }


    private fun updateMap(latLng: LatLng) {
        marker!!.position = latLng
        nMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
    }


    companion object {
        private const val DEFAULT_ZOOM = 10F
        private val defaultLocation = LatLng(-33.8523341, 151.2106085)
    }
}
