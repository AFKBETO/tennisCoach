package fr.efrei.footballtracker.createMatch

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.storage.FirebaseStorage
import fr.efrei.footballtracker.R

class ImageAdapter(private val urls: ArrayList<String>): RecyclerView.Adapter<ImageAdapter.ImageViewHolder>() {

    class ImageViewHolder(imageView: View, val context: Context): RecyclerView.ViewHolder(imageView) {
        private val imageView: ImageView = imageView.findViewById(R.id.match_photo)
        private var currentImage: Bitmap? = null
        private val storage: FirebaseStorage = FirebaseStorage.getInstance()


        fun bind(url: String) {
            val imageRef = storage.getReferenceFromUrl(url)
            imageRef.getBytes(ONE_MEGABYTE).addOnSuccessListener {
                currentImage = BitmapFactory.decodeByteArray(it, 0, it.size)
                imageView.setImageBitmap(currentImage)
            }.addOnFailureListener {
                // Handle any errors
            }


        }

        companion object {
            private const val ONE_MEGABYTE: Long = 1024 * 1024
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.photo_card, parent, false)
        return ImageViewHolder(view, parent.context)
    }

    override fun getItemCount(): Int {
        return urls.size
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bind(urls[position])
    }
}