package fr.efrei.footballtracker.createMatch

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration.ORIENTATION_PORTRAIT
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import fr.efrei.footballtracker.*
import fr.efrei.footballtracker.databinding.FragmentCreateMatchThirdBinding
import fr.efrei.footballtracker.firebaseRepositories.FirebaseStorageService
import fr.efrei.footballtracker.firebaseRepositories.Match
import fr.efrei.footballtracker.firebaseRepositories.MatchFirebaseService
import fr.efrei.footballtracker.matchHistory.MatchHistoryActivity
import fr.efrei.footballtracker.repositories.MatchDatabase
import fr.efrei.footballtracker.repositories.MatchSqlService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*


class CreateMatchFragment3 : Fragment() {
    var openCamera: FloatingActionButton? = null
    private var _binding: FragmentCreateMatchThirdBinding? = null
    private lateinit var matchFirebaseService: MatchFirebaseService
    private lateinit var resultLauncher: ActivityResultLauncher<Intent>
    private lateinit var storageService: FirebaseStorageService
    private lateinit var db: MatchDatabase
    private lateinit var match: Match


    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCreateMatchThirdBinding.inflate(inflater, container, false)
        getCameraPermission()
        matchFirebaseService = MatchFirebaseService
        db = MatchSqlService.getInstance()
        storageService = FirebaseStorageService
        auth = FirebaseAuth.getInstance()
        activity?.findViewById<View>(R.id.next_page)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.add_match)?.visibility = View.VISIBLE
        openCamera = binding.fab
        match = (activity as CreateMatchActivity).match

        val isPortrait = resources.configuration.orientation == ORIENTATION_PORTRAIT
        val spanCount = if (isPortrait) 2 else 4
        val gridLayoutManager = GridLayoutManager(requireContext(), spanCount)
        resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val imageBitmap = result.data?.extras?.get("data") as Bitmap
                // save locally
                val uuid = UUID.randomUUID().toString()
                val res = saveToInternalStorage(imageBitmap, uuid)
                println("res $res")

                viewLifecycleOwner.lifecycleScope.launch (Dispatchers.IO) {
                    val url = storageService.uploadPhoto(imageBitmap, uuid)
                    match.photos.add(url)
                    withContext(Dispatchers.Main) {
                        binding.imageRecyclerView.adapter?.notifyDataSetChanged()
                    }
                }

            }
        }
        openCamera?.setOnClickListener {
            if (auth.currentUser != null) {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                resultLauncher.launch(intent)
            } else {
                Toast.makeText(requireContext(), "You need to be logged in to add photos", Toast.LENGTH_SHORT).show()
            }
        }
        val imageAdapter = ImageAdapter(match.photos)
        binding.imageRecyclerView.adapter = imageAdapter
        binding.imageRecyclerView.layoutManager = gridLayoutManager

        activity?.findViewById<View>(R.id.add_match)?.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
                if (auth.currentUser != null) {
                    val result = matchFirebaseService.createMatch(match)
                    db.matchDao().insertMatchesWithPhoto(Match.convertToSql(result))
                } else {
                    match.id = UUID.randomUUID().toString()
                    db.matchDao().insertMatchesWithPhoto(Match.convertToSql(match))
                }
                val intent = Intent(requireContext(), MatchHistoryActivity::class.java)
                startActivity(intent)
            }
        }

        return binding.root
    }
    private fun getCameraPermission() {
        if (ContextCompat.checkSelfPermission(requireContext(),
                android.Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(android.Manifest.permission.CAMERA),
                PERMISSIONS_REQUEST_CAMERA
            )
        }
    }

    private fun saveToInternalStorage(bitmapImage: Bitmap, uuid: String): String? {

        return MediaStore.Images.Media.insertImage(
            requireActivity().contentResolver,
            bitmapImage,
            uuid,
            "Image of $uuid"
        )
    }

    companion object {
        private const val PERMISSIONS_REQUEST_CAMERA = 2
    }
}